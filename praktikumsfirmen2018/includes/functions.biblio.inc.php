<?php
/**
 * Created by IntelliJ IDEA.
 * User: TN62
 * Date: 18.12.2018
 * Time: 12:58
 */
function numberOfLines($text){
    if(strlen($text) <1){
        $lines = 2;
    }else{
        $lines = ceil((strlen($text)/32)) +1;
    }
    return $lines;
}