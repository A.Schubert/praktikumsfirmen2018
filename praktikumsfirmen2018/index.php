<?php
    include 'header.php';
    include 'includes/dbh.inc.php';
    include 'includes/functions.biblio.inc.php'
?>
<main>

        <?php
        $con = getDBConnect();
        if (isset($_GET['search'])){
            $search = $_GET['search'];
            $sql = 'CALL searchInDatabase(\''.$search.'\')';
            echo "<div class='headtext'><p> Ihre Suche nach <span style='color:white'>$search</span> ergab folgende Treffer:<a style='float:right;' href='index.php'> Komplette Seite Laden</a></p></div>";
        }else{
            $sql = 'CALL GetAll';
            echo " <div class='headtext'> <p class='headtext'> Alle Betriebe werden angezeigt:</p></div>";
        }
        $res = mysqli_query($con, $sql);
        echo " <div class=\"container\">";
        while($result = mysqli_fetch_assoc($res)){
            echo "<div class=\"box\">
                    <p id='number$result[id]'><a href=\"#top\">$result[id].</a></p>
                    <h2> $result[firmenname] </h2>
                    <div class=\"adress\">
                        <p> $result[strasse_hausnr], $result[plz] $result[ort]
                        <a href='https://www.google.de/maps/place/$result[strasse_hausnr],+$result[plz]+$result[ort]' title=\"Adresse auf Google Maps ansehen\" target=\"_blank\"><i class=\"fas fa-map-marker-alt\"></i></a></p>
                    </div>
                    <div class=\"web\">
                        <a href='$result[webseite]' target=\"_blank\">Homepage</a>";
                        if (!strpos($result['web_karriere'],'ttp')){
                            echo "<p>$result[web_karriere]</p>";
                        }else{
                            echo "<a href='$result[web_karriere]' target=\"_blank\">Karriere</a>";
                        }
                    echo "</div>
                <hr>
                    <div class=\"myInfo\">
                        <form action=\"includes/myInfoToDatabase.inc.php\" method='post'>
                        <input type='hidden' name='id' value='$result[id]'><label>";
                        $lines = numberOfLines($result['text']);
                       echo "Eigene Notizen: <textarea name='text' placeholder='Noch kein Text eingegeben' rows='$lines'>$result[text]</textarea></label>
                        <button class=\"submit\" type=\"submit\">Speichern</button>
                        </form>
                    </div>
            </div> ";
        }
        mysqli_close($con);
        ?>
</main>
<?php
    include 'footer.php';

