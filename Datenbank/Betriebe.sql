CREATE DATABASE `db_betriebsliste`;
USE `db_betriebsliste`;

CREATE TABLE `betriebe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firmenname` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `webseite` varchar(150) COLLATE latin1_general_ci DEFAULT NULL,
  `web_karriere` varchar(150) COLLATE latin1_general_ci DEFAULT NULL,
  `strasse_hausnr` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `plz` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `ort` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `text` text COLLATE latin1_general_ci,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `searchIndex` (`firmenname`,`strasse_hausnr`,`plz`,`ort`,`webseite`,`web_karriere`,`text`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1 COLLATE=LATIN1_GENERAL_CI;

INSERT INTO `betriebe` (`id`, `firmenname`, `webseite`, `web_karriere`, `strasse_hausnr`, `plz`, `ort`, `active`, `text`) VALUES
	(1, '42DIGITAL GmbH', 'https://www.42digital.de', 'https://www.42digital.de/jobs.html', 'Konsul-Smidt-Str. 8p', '28217', 'Bremen', 1, NULL),
	(2, 'abat AG', 'https://www.abat.de/de/', 'https://www.abat.de/de/studierende-praktikanten', 'An der Reeperbahn 10', '28217', 'Bremen', 1, NULL),
	(3, 'Airbus Operations GmbH', 'https://www.airbus.com/', 'http://company.airbus.com/careers/jobs-and-applications/search-for-vacancies.html?queryStr=&city=Bremen&country=de', 'Airbus-Allee 1', '28199', 'Bremen', 1, NULL),
	(4, 'Allgeier IT Solutions GmbH', 'https://www.allgeier-it.de/', 'https://www.allgeier-it.de/de/karriere/', 'Hans-Bredow-Str. 60', '28307', 'Bremen', 1, NULL),
	(5, 'AOT System GmbH', 'http://www.aotsystem.de/', 'BEWERBUNG ÜBER WIEBKE', 'Am Becketal 14', '28755', 'Bremen', 1, NULL),
	(6, 'apart.media GmbH', 'https://apart.media/', 'keine Infos gefunden', 'Damaschkestr. 10', '28307', 'Bremen', 1, NULL),
	(7, 'appfarms Gmbh & Co. KG', 'https://appfarms.com/', 'https://appfarms.com/bewerbung', 'Karl-Ferdinand-Braun-Straße 7', '28359', 'Bremen', 1, NULL),
	(8, 'artundweise GmbH', 'https://www.artundweise.de/', 'https://www.artundweise.de/karriere/', 'Domshof 8-12', '28195', 'Bremen', 1, NULL),
	(9, 'AS Abrechnungsstelle für Heil-Hilfs-und Pflegeberufe AG', 'https://as-bremen.de/', 'https://as-bremen.de/karriere.html', 'Am Wall 96 -98', '28195', 'Bremen', 1, NULL),
	(10, 'atacama Software GmbH', 'https://www.atacama.de/', 'https://www.atacama.de/karriere.html', 'Universitätsallee 15', '28359', 'Bremen', 1, NULL),
	(11, 'atbit Bremer Innovations- und Technologiegesellschaft mbH', 'https://www.atbit-konfigurator.de/index.php', 'https://www.atbit-konfigurator.de/index.php/jobs', 'Leerkämpe 8B', '28259', 'Bremen', 1, NULL),
	(12, 'ATO interactive Gmbh', 'https://www.ato.de/', 'https://www.ato.de/jobs', 'Otto-Lilienthal-Str. 27', '28199', 'Bremen', 1, NULL),
	(13, 'BBN Bremen Business Net GmbH', 'https://www.bbn.de/index.php', 'keine Infos gefunden', 'Fahrenheitstr. 7', '28359', 'Bremen', 1, NULL),
	(14, 'BEGO Gmbh & Co.KG', 'https://www.bego.com/de/', 'https://www.bego.com/de/karriere/stellenangebote/', 'Wilhelm-Herbst-Str. 1', '28359', 'Bremen', 1, NULL),
	(15, 'Benntec Systemtechnik GmbH', 'https://www.benntec.de/', 'https://www.benntec.de/karriere/arbeiten-bei-benntec', 'Karl-Ferdinand-Braun-Str. 7', '28359', 'Bremen', 1, NULL),
	(16, 'Bertling EDI Service & IT GmbH', 'https://besitec.eu/de/startseite/', 'https://besitec.eu/de/unternehmen/#karriere', 'Hafenstr. 55', '28217', 'Bremen', 1, NULL),
	(17, 'BLG LOGISTICS GROUP AG & Co. KG', 'https://www.blg-logistics.com/de', 'https://www.blg-logistics.com/de/karriere/schueler/schuelerpraktikum', 'Präsident-Kennedy-Platz 1', '28203', 'Bremen', 1, NULL),
	(18, 'Bremer Rechenzentrum Gmbh', 'https://www.brz.ag/', 'https://www.brz.ag/unternehmen/brz-jobangebote-bei-bremer-rechenzentrum/', 'Universitätsallee 5', '28359', 'Bremen', 1, NULL),
	(19, 'Bremer Straßenbahn AG', 'https://www.bsag.de/de/auskunft.html', 'https://www.bsag.de/de/unternehmen/jobs-ausbildung-karriere/ausbildung-praktikum.html', 'Flughafendamm 12', '28199', 'Bremen', 1, NULL),
	(20, 'BTC Business Technology Consulting AG', 'https://www.btc-ag.com/de/index.htm', 'https://www.btc-ag.com/de/schueler.htm', 'Am Weser-Terminal 1', '28217', 'Bremen', 1, NULL),
	(21, 'byte für byte', 'http://www.byte-fuer-byte.de/', 'keine Infos gefunden', 'Stader Str. 60', '28205', 'Bremen', 1, NULL),
	(22, 'C. Melchers GmbH & Co. KG', 'https://www.melchers.de/de/home/', 'https://www.melchers.de/de/karriere/ausbildung/', 'Schlachte 39/40', '28195', 'Bremen', 1, NULL),
	(23, 'cambio Bremen', 'http://www.cambio-carsharing.com/', 'https://www.cambio-carsharing.de/cms/carsharing/de/1/cms_f2_8/cms?cms_knuuid=ece7e863-f9c7-4a08-920d-e6f0bb131414', 'Humboldtstraße 131-137', '28203', 'Bremen', 1, NULL),
	(24, 'CLI holding GmbH', 'https://www.compasslog.com/de/home/', 'keine Infos gefunden', 'Franz-Stickan-Str. 4', '28197', 'Bremen', 1, NULL),
	(25, 'Commerz Systems GmbH', 'https://www.commerzbank.de', 'https://www.commerzbank.de/de/hauptnavigation/karriere/digitals/digitals.html', 'Schüsselkorb 5-11', '28195', 'Bremen', 1, NULL),
	(26, 'Comp-Pro Systemhaus GmbH', 'https://www.comp-pro.de/', 'https://www.comp-pro.de/karriere.html', 'Hoerneckestr. 19-21', '28217', 'Bremen', 1, NULL),
	(27, 'Consultix GmbH', 'https://www.consultix.de/', 'https://www.consultix.de/Karriere.html', 'Wachtstr.17-24', '28195', 'Bremen', 1, NULL),
	(28, 'CTS EVENTIM AG & Co. KGaA', 'https://corporate.eventim.de/', 'Bei Interesse Wiebke ansprechen', 'Contrescarpe 75A', '28195', 'Bremen', 1, NULL),
	(29, 'Cube-Tec International GmbH', 'https://www.cube-tec.com/de', 'https://www.cube-tec.com/de/firma/jobs#ausbildung', 'Anne-Conway-Str. 1', '28359', 'Bremen', 1, NULL),
	(30, 'CVS Ingenieurgesellschaft mbh', 'https://cvs.de/', 'https://alphaplan.de/stellenangebote/', 'Otto-Lilienthal-Str. 10', '28199', 'Bremen', 1, NULL),
	(31, 'dataport', 'https://www.dataport.de', 'https://www.dataport.de/Seiten/Karriere/%C3%9Cbersicht2.aspx', 'Utbremer Str. 90', '28217', 'Bremen', 1, NULL),
	(32, 'dbh Logistics IT AG', 'https://www.dbh.de/', 'https://www.dbh.de/karriere/', 'Martinistr. 47-49', '28195', 'Bremen', 1, NULL),
	(33, 'DECOIT® GmbH', 'https://www.decoit.de/de/home.html', 'https://www.decoit.de/de/jobs.html', 'Fahrenheitstr. 9', '28359', 'Bremen', 1, NULL),
	(34, 'Deutsche Telekom AG', 'https://www.telekom.com/de', 'https://www.t-systems.com/de/de/karriere/absolventen-studenten/praktikum/praktikum-88684', 'Stresemannstr. 4', '28207', 'Bremen', 1, NULL),
	(35, 'Die InformationsGesellschaft mbH', 'https://www.informationsgesellschaft.com/', 'keine Infos gefunden', 'Bornstr.12-13', '28195', 'Bremen', 1, NULL),
	(36, 'eCom webservices Tolkmitt & Sablotzki GbR', 'https://www.ecom-webservices.de/', 'https://www.ecom-webservices.de/jobs-bei-ecom/', 'Am Hohentorshafen 21a', '28197', 'Bremen', 1, NULL),
	(37, 'edicted GmbH', 'https://www.edicted.de/', 'keine Infos gefunden', 'Hinterm Sielhof 4-5', '28277', 'Bremen', 1, NULL),
	(38, 'EKB Container Logistik GmbH & Co. KG', 'http://www.ekb-containerlogistik.com/', 'http://www.ekb-containerlogistik.com/jobs.php', 'Richard-Dunkel-Str. 120', '28199', 'Bremen', 1, NULL),
	(39, 'encoway GmbH', 'https://www.encoway.de/', 'https://encoway.recruitee.com/', 'Buschhöhe 2', '28357', 'Bremen', 1, NULL),
	(40, 'engram GmbH', 'https://www.engram.de/', 'https://www.engram.de/karriere.html', 'Konsul-Smidt-Str. 8r', '28217', 'Bremen', 1, NULL),
	(41, 'ePhilos AG', 'https://ephilos.de', 'https://ephilos.de/karriere/praktikum.html', 'Fahrenheitstr. 7 - 9', '28359', 'Bremen', 1, NULL),
	(42, 'Fairtec Kommunikationstechnik GmbH', 'https://www.fairtec.de/de/', 'https://www.fairtec.de/de/karriere/#&panel1-3', 'Robert-Hooke-Str. 4', '28359', 'Bremen', 1, NULL),
	(43, 'Freie Hansestadt Bremen Aus- und Fortbildungszentrum(AFZ)', 'https://www.afz.bremen.de/', 'https://www.afz.bremen.de/ausbildung/praktika-734', 'Doventorscontrescarpe 172C', '28195', 'Bremen', 1, NULL),
	(44, 'Funkwerk loT GmbH', 'https://funkwerk-iot.com/', 'https://funkwerk-iot.com/jobs/', 'Konsul-Smidt-Str. 8l', '28217', 'Bremen', 1, NULL),
	(45, 'Gambio GmbH', 'https://www.gambio.de/', 'https://www.gambio.de/jobs.html', 'Parallelweg 30', '28219', 'Bremen', 1, NULL),
	(46, 'genese.de GmbH', 'https://www.genese.de/de/index.php', 'https://www.genese.de/de/stellenangebote.html', 'Leerkämpe 7', '28259', 'Bremen', 1, NULL),
	(47, 'GENU', 'http://www.genu.de/', 'http://www.genu.de/jobs', 'Hinterm Sielhof 24', '28277', 'Bremen', 1, NULL),
	(48, 'Gestalt und Form GmbH', 'http://www.mcgrip.de/web-map/eintrag/1344/', 'http://www.mcgrip.de/web-map/eintrag/1344/', 'Schwachhauser Heerstr. 2a', '28203', 'Bremen', 1, NULL),
	(49, 'Governikus GmbH & Co. KG', 'https://www.governikus.de/', 'https://www.governikus.de/karriere/offene-stellen/?L=398', 'Am Fallturm 9', '28359', 'Bremen', 1, NULL),
	(50, 'grow WerbeAgentur GmbH', 'https://www.grow-werbeagentur.de/', 'https://www.grow-werbeagentur.de/jobs/', 'Rembertiring 40', '28203', 'Bremen', 1, NULL),
	(51, 'GRÜN Software AG', 'https://www.gruen.net/', 'https://www.gruen.net/unternehmen/karriere-offene-stellen/#', 'Rigaer Str. 1', '28217', 'Bremen', 1, NULL),
	(52, 'HANSA-FLEX AG', 'https://www.hansa-flex.com/index.html', 'https://www.hansa-flex.com/karriere/praktika_und_abschlussarbeiten.html', 'Zum Panrepel 44', '28307', 'Bremen', 1, NULL),
	(53, 'Harris Orthogon GmbH', 'http://www.harris-orthogon.com/', 'keine Infos gefunden', 'Hastedter Osterdeich 222', '28207', 'Bremen', 1, NULL),
	(54, 'HEC GmbH', 'https://hec.de/', 'https://hec.de/karriere/', 'Konsul-Smidt-Str. 20', '28217', 'Bremen', 1, NULL),
	(55, 'HEPTACOM GmbH', 'https://www.heptacom.de/', 'keine Infos gefunden', 'Kurfürstenallee 130', '28211', 'Bremen', 1, NULL),
	(56, 'hmmh multimediahaus AG', 'https://www.hmmh.de/', 'Bei Interesse Wiebke ansprechen', 'Am Weser-Terminal 1', '28217', 'Bremen', 1, NULL),
	(57, 'IF-Com GmbH', 'https://www.if-com.de/home.php', 'keine Infos gefunden', 'Hermann-Ritter-Str. 108', '28197', 'Bremen', 1, NULL),
	(58, 'infomax websolutions GmbH', 'https://www.infomax-online.de/', 'https://www.infomax-online.de/karriere', 'Knochenhauerstr. 18/19', '28195', 'Bremen', 1, NULL),
	(59, 'INnUP Deutschland GmbH', 'https://www.innup.de/', 'keine Infos gefunden', 'Theodor-Heuss-Allee 21', '28215', 'Bremen', 1, NULL),
	(60, 'Institut für Seeverkehrswirtschaft und Logistik (ISL)', 'https://www.isl.org/index.php/de', 'https://www.isl.org/de/ueber-isl/ausschreibungen', 'Universitätsallee 11 - 13', '28359', 'Bremen', 1, NULL),
	(61, 'KAEFER Isoliertechnik GmbH &Co. KG', 'https://de.kaefer.com/Home.html', 'https://de.kaefer.com/Praktika.html', 'Marktstr. 2', '28195', 'Bremen', 1, NULL),
	(62, 'KPS Payment GmbH & Co. KG', 'http://www.kps-payment.de/', 'https://kpsgruppe.softgarden.io/de/vacancies', 'Contrescarpe 75A', '28195', 'Bremen', 1, NULL),
	(63, 'Lexzau, Scharbau GmbH & Co. KG', 'https://www.leschaco.com/de/start.html', 'https://www.leschaco.com/de/karriere/was-wir-bieten.html', 'Kap-Horn-Str. 18', '28237', 'Bremen', 1, NULL),
	(64, 'lindbaum GbR', 'https://www.lindbaum.de/', 'https://www.lindbaum.de/', 'Brautstraße 1-2', '28199', 'Bremen', 1, NULL),
	(65, 'MEKO-S GmbH', 'https://www.meko-s.de/', 'https://www.meko-s.de/unternehmen/#jobs', 'Lise-Meitner-Str. 6', '28359', 'Bremen', 1, NULL),
	(66, 'MERENTIS GmbH', 'https://www.merentis.com/', 'https://www.merentis.com/unternehmen/jobs/', 'Kurfürstenallee 130', '28211', 'Bremen', 1, NULL),
	(67, 'MSP MEDIEN SYSTEMPARTNER GMBH & CO. KG', 'https://www.medien-systempartner.de/', 'https://www.medien-systempartner.de/karriere/', 'Martinistr. 33', '28195', 'Bremen', 1, NULL),
	(68, 'neusta GmbH', 'https://www.team-neusta.de/', 'https://www.team-neusta.de/jobs/test-aus/', 'Konsul-Smidt-Str. 24', '28217', 'Bremen', 1, NULL),
	(69, 'neusta mobile solutions GmbH', 'http://www.neusta-ms.de/', 'http://www.neusta-ms.de/jobs/', 'Konsul-Smidt-Str. 24', '28217', 'Bremen', 1, NULL),
	(70, 'NXTGN Music Technology GmbH', 'https://www.ujam.com/', 'https://www.ujam.com/company/#jobs', 'An der Reeperbahn 6', '28217', 'Bremen', 1, NULL),
	(71, 'Oraise GmbH', 'https://www.oraise.com/', 'https://www.oraise.com/karriere/', 'Mary-Somerville-Straße 10', '28359', 'Bremen', 1, NULL),
	(72, 'Orga-Support GmbH', 'https://www.seghorn.de/', 'https://www.seghorn.de/karriere/bei-uns-arbeiten.html', 'Stresemannstr. 60', '28207', 'Bremen', 1, NULL),
	(73, 'OTARIS Interactive Services GmbH', 'https://www.otaris.de/', 'https://www.otaris.de/karriere/', 'Fahrenheitstr. 7', '28359', 'Bremen', 1, NULL),
	(74, 'Prodware Deutschland AG', 'https://www.prodware.de/', 'https://www.prodware.de/karriere/einstieg-bei-prodware', 'Otto-Lilienthal-Str. 27', '28199', 'Bremen', 1, NULL),
	(75, 'PTS Group AG', 'https://www.ptsgroup.de/', 'https://www.ptsgroup.de/karriere/', 'Cuxhavener Str. 10a ', '28217', 'Bremen', 1, NULL),
	(76, 'QSI – Quality Services International GmbH', 'https://www.qsi-q3.de/', 'https://www.qsi-q3.de/karriere/', 'Flughafendamm 9A', '28199', 'Bremen', 1, NULL),
	(77, 'Ravenworks GbR', 'http://www.ravenworks.de/ravenworks.html', 'keine Infos gefunden', 'Mary-Astell-Str. 10', '28359', 'Bremen', 1, NULL),
	(78, 'SSI SCHÄFER FRITZ SCHÄFER GMBH', 'https://www.ssi-schaefer.com/de-de', 'https://www.ssi-schaefer.com/de-de/unternehmen/karriere/ausbildung-und-studium-in-bremen-459628', 'Hanna-Kunath-Str. 15', '28199', 'Bremen', 1, NULL),
	(79, 'stoll informationssysteme', 'https://www.stoll-is.de/1home.aspx', 'keine Infos gefunden', 'Am Winterhafen 3', '28217', 'Bremen', 1, NULL),
	(80, 'STUTE Logistics (AG&Co.) KG', 'https://www.stute.de/', 'https://www.stute.de/karriere/arbeitgeberprofil/', 'Hans-Böckler-Str. 48', '28217', 'Bremen', 1, NULL),
	(81, 'szenaris GmbH', 'https://www.szenaris.com/de/home', 'https://www.szenaris.com/de/karriere', 'Otto-Lilienthal-Str. 1', '28199', 'Bremen', 1, NULL),
	(82, 'TA-LOGISTIC Software GmbH', 'https://www.ta-logistic.de/', 'https://www.ta-logistic.de/ueber-uns/jobs/', 'Straubinger Str. 5', '28219', 'Bremen', 1, NULL),
	(83, 'Trenz GmbH', 'https://trenz.de/', 'https://www.trenz.ag/Rubrik/212/Unser_Unternehmen/Karriere/index.html', 'Neidenburger Str. 14', '28207', 'Bremen', 1, NULL),
	(84, 'Univention GmbH', 'https://www.univention.de/', 'https://www.univention.de/ueber-uns/karriere/', 'Mary-Somerville-Str. 1', '28359', 'Bremen', 1, NULL),
	(85, 'Webcustoms IT Solutions GmbH', 'https://webcustoms.de/', 'https://webcustoms.de/jobs-karriere/', 'An der Reeperbahn 6', '28217', 'Bremen', 1, NULL),
	(86, 'WebMen Internet GmbH', 'https://www.webmen.de/Home.html', 'https://www.webmen.de/Internetagentur-Bremen-Stellenangebote.html', 'Tiefer 2', '28195', 'Bremen', 1, NULL);

DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetAll`()
BEGIN
SELECT firmenname,strasse_hausnr, plz, ort, webseite, web_karriere, text, id FROM betriebe WHERE active = 1;
END//
DELIMITER ;

DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertText`(
	IN `input` TEXT,
	IN `fid` INT
)
BEGIN
UPDATE betriebe SET betriebe.text = input WHERE betriebe.id = fid;
END//
DELIMITER ;

DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `searchInDatabase`(IN `searchTerm` VARCHAR(20) CHARSET utf8)
    NO SQL
BEGIN
SELECT firmenname,strasse_hausnr, plz, ort, webseite, web_karriere, text, id FROM betriebe WHERE active = 1 AND MATCH (firmenname,strasse_hausnr, plz, ort, webseite, web_karriere, text) AGAINST (searchTerm IN BOOLEAN MODE) ;
END//
DELIMITER ;