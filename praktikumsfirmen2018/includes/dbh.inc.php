<?php
/**
 * Created by IntelliJ IDEA.
 * User: Andree
 * Date: 12.12.2018
 * Time: 19:25
 */

const DB_HOST = "localhost";
const DB_USERNAME = "root";
const DB_PASSWORD = "";
const DB_DATABASE = "db_betriebsliste";
const DB_CHARSET = 'utf8';

function getDBConnect(){
    $con = mysqli_connect(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
    mysqli_set_charset($con,DB_CHARSET);
    if(!$con){
        die("Connection failed!".mysqli_connect_error());
    }
    return $con;
}